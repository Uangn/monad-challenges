{-# LANGUAGE MonadComprehensions #-}
{-# LANGUAGE RebindableSyntax #-}

module Set1 where

import Control.Arrow (Arrow (..))
import MCPrelude

type Gen r = Seed -> (r, Seed)

---- 1
fiveRands :: [Integer]
fiveRands = fst $ repRandom (replicate 5 rand) (mkSeed 1)

---- 2
generalA :: (b -> c) -> (a -> (b, a)) -> a -> (c, a)
generalA f g = first f . g

randLetter :: Gen Char
randLetter = generalA toLetter rand

randString3 :: String
randString3 = fst $ repRandom (replicate 3 randLetter) (mkSeed 1)

---- 3
randEven :: Seed -> (Integer, Seed)
randEven = generalA (* 2) rand

randOdd :: Seed -> (Integer, Seed)
randOdd = generalA (+ 1) randEven

randTen :: Seed -> (Integer, Seed)
randTen = generalA (* 2) rand

---- 4
gApp :: (s -> (a -> b, s)) -> (s -> (a, s)) -> s -> (b, s)
gApp fab fa x =
    let (ab, s) = fab x
     in generalA ab fa s

generalB' :: (a -> b -> c) -> (s -> (a, s)) -> (s -> (b, s)) -> s -> (c, s)
generalB' c f g = c `generalA` f `gApp` g

generalB :: (a -> b -> c) -> (s -> (a, s)) -> (s -> (b, s)) -> s -> (c, s)
generalB c f g x =
    let (r, s) = f x
        (r', s') = g s
     in (c r r', s')

generalPair :: Gen a -> Gen b -> Gen (a, b)
generalPair = generalB (,)

randPair :: Gen (Char, Integer)
randPair = generalPair randLetter rand

---- 5
repRandom :: [Gen a] -> Gen [a]
repRandom [] s = mkGen [] s
repRandom (f : fs) s =
    let (r, s') = f s
     in generalA (r :) (repRandom fs) s'

---- 6
genTwo :: Gen a -> (a -> Gen b) -> Gen b
genTwo f g x =
    let (r, s) = f x
     in g r s

mkGen :: a -> Gen a
mkGen r s = (r, s)
